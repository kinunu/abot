<?php

namespace App\Http\Controllers;

use App\Conversations\ExampleConversation;
use Illuminate\Http\Request;
use Mpociot\BotMan\BotMan;
use GuzzleHttp\Client;
use App\User;
use App\Transaction;

//https://api.telegram.org/bot406295417:AAHHLNkCveMVAKnD8WYxsvIcwHyCRxvOgqY/setWebhook?url=https://e20238e3.ngrok.io

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');
        $botman->verifyServices(env('TOKEN_VERIFY'));

        $botman->hears('hello', function(BotMan $bot){
            $bot->types();
            $bot->reply('Hi there :)');
        });

        $botman->hears('/start', function(BotMan $bot){
            $bot->types();
            $user = $bot->getUser();
            //welcome Messasge
            $bot->reply('Welcome '.$user->getUsername().' to Test Bot! :) ');
            //Message 1.
            $bot->reply('Here you get 8% on your Investment each day. This will give you a total of 200% after 25 days. ');
            //Message 2.
            $bot->reply('These returns are made possible by trading a wide range of Cryptocurrencies.Our programmers and traders have worked together on the algorithms that make these profits possible for you ALL.');
            //Message 3.
            $bot->reply('Make your first deposit click: /invest');
        });

        $botman->hears('/invest {amount}', function ($bot, $amount) {
            $bot->types();
            $user = $bot->getUser();
            $msg = $this->invest($user->getId(),$amount);
            $bot->reply($msg);
            $bot->reply("Thank you for investing with us");
            $bot->reply("Click /history to check your history");
        });

        $botman->hears('/withdraw {amount}', function ($bot, $amount) {
            $bot->types();
            $user = $bot->getUser();
            $bot->reply("Processing your withdrawal");
            $msg = $this->withdraw($user->getId(), $amount);
            $bot->reply($msg);
        });

        $botman->hears('/my_balance', function ($bot) {
            $bot->types();
            $user = $bot->getUser();
            $bot->reply("Processing your withdrawal");
            $bal = $this->getBal($user->getId());
            $bot->reply("Your Balance is :".$bal);
        });

        $botman->hears('/reinvest', function ($bot) {
            $bot->types();
            $user = $bot->getUser();
            $bot->reply("Processing your request");
            $msg = $this->reinvest($user->getId());
            $bot->reply($msg);
            $bot->reply("Thank you");
        });

        $botman->hears('/history', function ($bot) {
            $bot->types();
            $user = $bot->getUser();
            $res = $this->History($user->getId());
            $bot->reply($res);
        });

        $botman->hears('/referal_system', function ($bot) {
            $bot->types();
            $user = $bot->getUser();
            $res = $this->Referal($user->getId());
            $bot->reply($res);
        });

        $botman->hears('/support', function ($bot) {
            $bot->types();
            $user = $bot->getUser();
            $bot->reply('Have a question '.$user->getUsername().'!');
            $bot->reply('You can contact our support team');
            $bot->reply('@SupportTestBot');
        });

        $botman->hears('Get {currency} rates', function ($bot, $currency) {
            $bot->types();
            $results = $this->getCurrency($currency);
            $bot->reply($results);
        });

        $botman->fallback(function(BotMan $bot) {
            $bot->types();
            $bot->reply('Sorry, I did not get you there... Could you tell me how i can help you');
        });

        $botman->listen();
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }

    public static function getCurrency($currency) {
        $client = new Client();
        $uri = 'http://api.fixer.io/latest?base='.$currency;
        $response = $client->get($uri);
        $results = json_decode($response->getBody()->getContents());
        $date = date('d F Y', strtotime($results->date));
        $data = "Here's the exchange rates based on ".$currency." currency\nDate: ".$date."\n";
        foreach($results->rates as $k => $v) {
            $data .= $k." - ".$v."\n";
        }
        return $data;
    }

    public static function invest($id, $amount){
        //return 'Thanks';
        self::checkUser($id);
        self::CreateTransaction($id,$amount,'invest');
    }

    public static function checkUser($id)
    {
        return User::updateOrCreate(['telegram_id'=>$id],
            ['telegram_id'=>$id]
        );
    }

    private static function CreateTransaction($id,$amount,$type)
    {
        $user = User::whereTelegram_id($id)->get()->first();
        $t = new Transaction();
        $t->user_id = $user->id;
        $t->telegram_id = $user->telegram_id;
        $t->type = $type;
        $t->amount = $amount;
        $t->save();
    }

    public static function withdraw($id, $amount){
        $bal = self::getBal($id);
        if($bal<=0){
            return 'Your account balance is not sufficient';
        }elseif ($bal>0 && $amount>$bal){
            return 'You have only '.$bal.' in your account';
        }else{
            if(self::CreateTransaction($id,$amount,'withdraw')){
                return 'Your withdrawal of '.$amount . 'was successful';
            }else{
                //return 'Ooops :( you missed something';
            }
        }
    }

    public static function getBal($id){
        self::checkUser($id);
        $in = Transaction::whereTelegram_id($id)->whereType('invest')->sum('amount');
        $out = Transaction::whereTelegram_id($id)->whereType('withdraw')->sum('amount');
        $bal = $in-$out;
        return $bal;//$balance;
    }

    public static function reinvest($user){
        $bal = self::getBal($user);
        if(0.01>$bal){
            return "Your balance is below the minimum investment of 0.01";
        }else{
            return "You sure you wish to invest: $bal";
        }
    }

    public static function History($user){
        $transactions = Transaction::whereTelegram_id($user)->get();
        $i=0;
        $data = "\nid \t Amount \t Date \n";
        foreach ($transactions as $transaction){
            $i+=1;
            $data .="".$transaction->id." \t ".$transaction->amount." \t ".$transaction->created_at." \n";
        }
        return $data;
    }

    public static function Referal($user){
        return 'You have not referred anyone yet!';
    }




}
